<?php

namespace App\POO\Ex00;

use App\Resources\Classes\Lannister\Lannister;

  class Tyrion extends Lannister
  {
      private $size = 'Short';

      public function getSize(): string
      {
          return $this->size;
      }

      public function sleepWith($object)
      {
          if (is_subclass_of($object, 'App\Resources\Classes\Lannister\Lannister')) {
              echo "Not even if I'm drunk !\n";
          } else {
              echo "Let's do this.\n";
          }
      }

      protected function announceBirth(): void
      {
          if ($this->needBirthAnnouncement) {
              echo parent::BIRTH_ANNOUNCEMENT . "My name is Tyrion\n";
          }
      }
  }
