<?php

namespace App\Poo\Ex06;

use App\POO\Ex05\IFighter;

class UnholyFactory
{
    public array $ls_fighter = [];

    public function absorb(IFighter $man): self
    {
        if ($man instanceof Fighter) {
            $type = $man->returnType();

            if (array_key_exists($type, $this->ls_fighter)) {
                echo "(Factory already absorbed a fighter of type $type)\n";
            } else {
                echo "(Factory absorbed a fighter of type $type)\n";
                $this->ls_fighter[$type] = $man;
            }
        } else {
            echo "(Factory can't absorb this, it's not a fighter)\n";
        }

        return $this;
    }

    public function fabricate(string $fab)
    {
        if (array_key_exists($fab, $this->ls_fighter)) {
            echo "(Factory fabricates a fighter of type $fab)\n";

            return $this->ls_fighter[$fab];
        } else {
            echo "(Factory hasn't absorbed any fighter of type $fab)\n";

            // return null;
        }
    }
}

  // static $ls_fighters =[];

  // array_push(self::$ls_fighters, $fighters);
  //   return self::$ls_fighters;

//   public function absorb($fighters) {

//     foreach (self :: $ls_fighters as $fighter)
//     {
//       if (method_exists($fighter, 'fight'))
//       {
//         if (method_exists($fighter, 'llama'))
//         {
//         echo "(Factory hasn't absorbed any fighter of type llama)";
//         }
//         else{
//         echo "(Factory absorbed a fighter of type", get_parent_class('archer', 'soldier', 'assassin','foot soldier');
//           }

//       }else
//       {
//       echo "(Factory can't absorb this, it's not a fighter)";
//       }
//     }
//   }
// }
