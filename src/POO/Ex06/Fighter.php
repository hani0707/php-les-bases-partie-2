<?php

namespace App\Poo\Ex06;

use App\POO\Ex05\IFighter;

abstract class Fighter implements IFighter
{
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function returnType(): string
    {
        return $this->type;
    }

    // abstract public function fight(string $target = ''): void;
}
