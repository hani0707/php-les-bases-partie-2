<?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    public static $ls_recruits = [];

    public function recruit($recruits)
    {
        array_push(self::$ls_recruits, $recruits);

        return self::$ls_recruits;
    }

    public function fight()
    {
        foreach (self::$ls_recruits as $fighters) {
            if (method_exists($fighters, 'fight')) {
                $fighters->fight();
            }
        }
    }
}
