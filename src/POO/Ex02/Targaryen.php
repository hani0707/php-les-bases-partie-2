<?php

namespace App\POO\Ex02;

class Targaryen
{
    public function resistsFire(): bool
    {
        return false;
    }

    public function getBurned()
    {
        if ($this->resistsFire() == false) {
            return 'burns alive';
        } else {
            return 'emerges naked but unharmed';
        }
    }
}
