<?php

function ft_split($chaine)
{
    $tab = preg_split("/\W/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
    sort($tab);

    return $tab;
}
