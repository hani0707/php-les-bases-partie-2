<?php

if ($argc == 2) {
    $chaine = $argv[1];

    if (preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY) != false) {
        $tab = preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY);

        $jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

        $mois = ['Janvier' => 1, 'Fevrier' => 2, 'Mars' => 3, 'Avril' => 4, 'Mai' => 5, 'Juin' => 6, 'Juillet' => 7, 'Aout' => 8, 'Septembre' => 9, 'Octobre' => 10, 'Novembre' => 11, 'Decembre' => 12];

        if (in_array(ucfirst(strtolower($tab[0])), $jour) && array_key_exists(ucfirst(strtolower($tab[2])), $mois)) {
            $Chiffrejour = $tab[1];
            $Chiffremois = $mois[ucfirst(strtolower($tab[2]))];
            // var_dump($tab[2]);
            $ChiffreAnee = $tab[3];
            $temp = preg_split('/:/', $tab[4], -1, PREG_SPLIT_NO_EMPTY);

            if ($temp[0] < 24 && $temp[1] < 60 && $temp[2] < 60) {
                $heure = true;
            } elseif ($temp[0] == 24 && $temp[1] == 60 && $temp[2] == 60) {
                $heure = true;
            } else {
                $heure = false;
            }

            if (checkdate($Chiffremois, $Chiffrejour, $ChiffreAnee) && ($heure == true)) {
                $nom = $Chiffremois . '/' . $Chiffrejour . '/' . $ChiffreAnee . ' ' . $temp[0] . ':' . $temp[1] . ':' . $temp[2];
                $date = new DateTime($nom, new DateTimeZone('CET'));
                $result = $date->getTimestamp();
                echo $result . "\n";
            } else {
                echo "Wrong Format\n";
            }
        } else {
            echo "Wrong Format\n";
        }
    } else {
        echo '';
    }
} else {
    echo "Wrong Format\n";
}
