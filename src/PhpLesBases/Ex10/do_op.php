<?php

// controle si le nombre d'arguments est egal a 4
if ($argc != 4) {
    echo "Incorrect Parameters\n";
} else {
    // recherche les nombres
    preg_match("!\d+!", $argv[1], $debut);
    preg_match('!\d+!', $argv[3], $fin);

    // recherche les signes de ponctuations
    preg_match('/\+|\*|\/|\-|\%/', $argv[2], $signe);

    // controle de la coherence des arguments
    if ($debut != false && $fin != false && $signe != false) {
        $a = $debut[0];
        $b = $fin[0];
        $signe = $signe[0];

        if ($signe == '+') {
            echo $a + $b . "\n";
        } elseif ($signe == '-') {
            echo $a - $b . "\n";
        } elseif ($signe == '*') {
            echo $a * $b . "\n";
        } elseif ($signe == '/') {
            if ($b != 0) {
                echo $a / $b . "\n";
            } else {
                echo "0\n";
            }
        } elseif ($signe == '%') {
            echo $a % $b . "\n";
        }
    } else {
        echo "Incorrect Parameters\n";
    }
}
