<?php

// recupere l'argument en chaine de caracteres
if ($argc == 2) {
    $chaine = $argv[1];

    if (preg_match_all('/\+|\*|\/|\-|\%/', $chaine, $signe) != false) {
        $tab = preg_split('/\+|\*|\/|\-|\%/', $chaine, -1, PREG_SPLIT_NO_EMPTY);

        // var_dump($tab);
        // var_dump(count($signe[0]));

        if ($tab != false && count($signe[0]) == 1) {
            // var_dump($signe[0]);

            $sign = $signe[0][0];
            $a = $tab[0];
            $b = $tab[1];

            if (is_numeric($a) && is_numeric($b)) {
                if ($sign == '+') {
                    echo $a + $b . "\n";
                } elseif ($sign == '-') {
                    echo $a - $b . "\n";
                } elseif ($sign == '*') {
                    echo $a * $b . "\n";
                } elseif ($sign == '/') {
                    if ($b != 0) {
                        echo $a / $b . "\n";
                    } else {
                        echo "0\n";
                    }
                } elseif ($sign == '%') {
                    echo $a % $b . "\n";
                }
            } else {
                echo "Syntax Error\n";
            }
        } else {
            echo "Syntax Error\n";
        }
    } else {
        echo "Syntax Error\n";
    }
} else {
    echo "Incorrect Parameters\n";
}
