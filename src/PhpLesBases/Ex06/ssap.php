<?php

// var_dump($argv);

$list = [];

function ft_split($chaine)
{
    $tab = preg_split("/[^\S\r\n]/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
    sort($tab);

    return $tab;
}

for ($i = 1; $i < $argc; ++$i) {
    foreach (ft_split($argv[$i]) as $mot) {
        array_push($list, $mot);
    }
}

sort($list, SORT_STRING);

foreach ($list as $value) {
    echo $value . "\n";
}
