<?php

const DIR = 'private';
const FILE_DIR = 'private/passwd';

const ERROR_OUTPUT = "ERROR\n";
const SUCCES_OUTPUT = "OK\n";

const PASSWORD_INPUT_NAME = 'passwd';
const SUBMIT_INPUT_NAME = 'submit';
const SUBMIT_INPUT_VALUE = 'OK';
const USERNAME_INPUT_NAME = 'login';

if (
    ($_POST[SUBMIT_INPUT_NAME] ?? null) !== SUBMIT_INPUT_VALUE
    || empty(($_POST[USERNAME_INPUT_NAME] ?? null))
    || empty(($_POST[PASSWORD_INPUT_NAME] ?? null))
) {
    echo ERROR_OUTPUT;
    exit;
}

if (!file_exists(DIR)) {
    mkdir(DIR, 0755, true);
}

if (!file_exists(FILE_DIR)) {
    file_put_contents(FILE_DIR, '');
}

$login = $_POST[USERNAME_INPUT_NAME];
$credentialDatabase = unserialize(file_get_contents(FILE_DIR)) ?: [];

foreach ($credentialDatabase as [USERNAME_INPUT_NAME => $userLogin]) {
    if ($userLogin === $login) {
        echo ERROR_OUTPUT;
        exit;
    }
}

$credentialDatabase[] = [USERNAME_INPUT_NAME => $login, PASSWORD_INPUT_NAME => password_hash($_POST[PASSWORD_INPUT_NAME], PASSWORD_DEFAULT)];
file_put_contents(FILE_DIR, serialize($credentialDatabase));

echo SUCCES_OUTPUT;
