<?php

function ft_is_sort($tab)
{
    // je copie mon tableau dans la variable $temp
    $temp = $tab;

    // je trie la copie de mon tableau
    sort($temp);

    // je compare l'original avec la copie trie
    if ($temp == $tab) {
        return true;
    } else {
        return false;
    }
}
